<?php

function root($n, $max = null, $min = null)
{
  if (empty($max)) {
    $n = floatval($n);
    $max = $n;
    $min = 0;
  }

  $estimated = ($max + $min) / 2;
  $calc = $estimated * $estimated;

  if (abs($n - $calc) < 0.01) {
    $value = $estimated;
  } else {
    if ($calc > $n) {
      $value = root($n, $estimated, $min);
    } elseif ($calc < $n) {
      $value = root($n, $max, $estimated);
    }
  }

  return $value;
}

printf("Result: %s\n", root(10));
