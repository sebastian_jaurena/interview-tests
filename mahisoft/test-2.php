<?php

function max_profit($stock_rates)
{
  $days = count($stock_rates);

  $max_profit = 0;
  $results = [];

  for ($buy_date = 0; $buy_date < $days; $buy_date++) {
    $buy_value = $stock_rates[$buy_date];

    for ($sell_date = $buy_date + 1; $sell_date < $days; $sell_date++) {
      $sell_value = $stock_rates[$sell_date];
      $profit = $sell_value - $buy_value;

      $result = [
        'buy' => $buy_date,
        'sell' => $sell_date,
        'profit' => $profit
      ];

      if ($max_profit === $profit) {
        $results[] = $result;
        $max_profit = $profit;
      } else if ($profit > $max_profit) {
        $results = [$result];
        $max_profit = $profit;
      }
    }
  }

  return $results;
}

$results = max_profit([1, 2, 1, 5, 2.1, 6, 3]);

printf("Results:\n");
foreach ($results as $s) {
  printf("Buy date: %s / Sell date: %s - Profit: %s\n", $s['buy'], $s['sell'], $s['profit']);
}
