# Mahisoft

## Sqrt

Write a function to calculate sqrt of integer number

[Test](https://bitbucket.org/sebastian_jaurena/interview-tests/src/master/mahisoft/test-1.php)

## Max stock profit

Given an array of stock values along days, write a function to calculate max profit

[Test](https://bitbucket.org/sebastian_jaurena/interview-tests/src/master/mahisoft/test-2.php)

## Letters count

Given a text write a function to count letters

[Test](https://bitbucket.org/sebastian_jaurena/interview-tests/src/master/mahisoft/test-3.php)