<?php

function chars_count($text)
{
  $letters = [];
  
  for ($i = 0; $i < strlen($text); $i++) {
    $letter = $text[$i];

    if (isset($letters[$letter])) {
      $letters[$letter]++;
    } else {
      $letters[$letter] = 1;
    }
  }

  $output = '';

  foreach ($letters as $letter => $nr) {
    $output .= ($nr > 1) ? $nr . $letter : $letter;
  }

  return $output;
}

$result = chars_count('ABBBBCFLL');

printf("Result: %s\n", $result);
